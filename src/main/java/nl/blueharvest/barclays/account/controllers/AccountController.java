package nl.blueharvest.barclays.account.controllers;

import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import nl.blueharvest.barclays.account.model.Account;
import nl.blueharvest.barclays.account.services.AccountService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Optional;

@RequiredArgsConstructor
@RestController
public class AccountController {

    @NonNull
    private final AccountService accountService;

    @GetMapping(value = "/accounts")
    public ResponseEntity<List<Account>> getAccountListByCustomer(@RequestParam("customer_id") final Long customerId) {
        return ResponseEntity.ok().body(accountService.getAllAccountsByCustomerId(customerId));
    }

    @GetMapping(value = "/accounts/{account_id}")
    public ResponseEntity getAccount(@RequestParam("account_id") final Long accountId) {
        Optional<Account> returnedAccount = accountService.getAccountById(accountId);
        return returnedAccount.isPresent() ? ResponseEntity.ok().body(returnedAccount.get()) : (ResponseEntity) ResponseEntity.notFound();
    }
}
