package nl.blueharvest.barclays.account.services;

import lombok.extern.slf4j.Slf4j;
import nl.blueharvest.barclays.account.model.Account;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Slf4j
@Service
public class AccountService {

    public Optional<Account> getAccountById(final Long accountId) {
        return mockAccountsList()
                .stream()
                .filter( account -> account.getAccountId().equals( accountId ) )
                .findFirst();
    }

    public List<Account> getAllAccountsByCustomerId(final Long customerId) {
        return mockAccountsList()
                .stream()
                .filter( account -> account.getCustomerId().contains( customerId ) )
                .collect( Collectors.toList() );
    }

    private List<Account> mockAccountsList() {

        List<Account> returnList = new ArrayList<>();

        returnList.add(
                Account.builder()
                        .accountId(1l)
                        .accountTitle("Savings account")
                        .customerId( createCustomerList( 1l ) )
                        .entitled("L. Smith")
                        .build()
        );

        returnList.add(
                Account.builder()
                        .accountId(2l)
                        .accountTitle("Personal account")
                        .customerId( createCustomerList( 1l ) )
                        .entitled("L. Smith")
                        .build()
        );

        returnList.add(
                Account.builder()
                        .accountId(3l)
                        .accountTitle("Cj Account")
                        .customerId( createCustomerList( 1l, 2l ) )
                        .entitled("L. Smith & B. Fire")
                        .build()
        );

        returnList.add(
                Account.builder()
                        .accountId(4l)
                        .accountTitle("Personal account")
                        .customerId( createCustomerList( 2l ) )
                        .entitled("B. Fire")
                        .build()
        );

        returnList.add(
                Account.builder()
                        .accountId(5l)
                        .accountTitle("Savings Account")
                        .customerId( createCustomerList( 3l ) )
                        .entitled("A. Gathering")
                        .build()
        );

        returnList.add(
                Account.builder()
                        .accountId(6l)
                        .accountTitle("Personal Account")
                        .customerId( createCustomerList( 3l ) )
                        .entitled("A. Gathering")
                        .build()
        );

        returnList.add(
                Account.builder()
                        .accountId(7l)
                        .accountTitle(null)
                        .customerId( createCustomerList( 3l ) )
                        .entitled("A. Gathering")
                        .build()
        );

        return returnList;
    }

    private List<Long> createCustomerList(Long... customers) {
        List<Long> returnList = new ArrayList<>();
        for (Long customer : customers) {
            returnList.add(customer);
        }
        return returnList;
    }

}
