package nl.blueharvest.barclays.account.model;

import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import lombok.Builder;
import lombok.Getter;
import org.springframework.lang.Nullable;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;

@Getter
@Builder
@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
public class Account {

    @NotNull
    private final Long accountId;

    @NotNull
    @Size(min = 1, max = 2)
    private final List<Long> customerId;

    @Nullable
    @Size(max = 100)
    private final String accountTitle;

    @NotNull
    @Size(min = 5, max = 100)
    private final String entitled;

}
