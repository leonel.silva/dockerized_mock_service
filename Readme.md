## Mock Service - Spring Boot Dockerized

This is meant to be a small mocked service to be deployed in a Kubernetes cluster.

First build your docker image

Make sure you have permission to run the script

```
chmod +x *.sh
```
And then run the build to generated the docker image using maven:
```
./build.sh
```
Then you can run the docker container
```
./run.sh
```

------

Afterwards, make sure you are going to stop and remove the containers

```
./stop_and_remove.sh
```

______

### API

The context path of this application is /accounts/v1

The basic controller is /account

There is 2 services in this api

http://localhost:8989/accounts/v1/accounts?customer_id=[number]

http://localhost:8989/accounts/v1/accounts/{account_id}

There are 7 accounts id (1 to 7)

And 3 customers ids (1 to 3)

1 - L. Smith,
2 - B. Fire,
3 - A. Gathering